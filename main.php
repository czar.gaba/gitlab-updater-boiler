<?php 
/**
 * Plugin Name:       Gitlab Updater Boiler Template
 * Plugin URI:        https://gitlab.com/czar.gaba/gitlab-updater-boiler
 * GitLab Plugin URI: https://gitlab.com/czar.gaba/gitlab-updater-boiler
 * Description:       Plugin for Plugin Update using GitLab
 * Version:           1.0.14
 * Author:            Czar Encarnacion Gaba
 * Author URI:        https://gitlab.com/czar.gaba/
 * License:           GPL-2.0+
 * Network:           false
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * 
 */


require_once 'wp-gitlab-updater/plugin-updater.php';    
new Moenus\GitLabUpdater\PluginUpdater( [
    'slug' => 'gitlab-updater-boiler', //recommended name -> Folder name
    'plugin_base_name' => 'gitlab-updater-boiler/main.php', // base php file of the plugin along with the slug
    'access_token' => 'YB23oMWx6wDuzmozCoT4', // for private repo 
    'gitlab_url' => 'https://gitlab.com', // GitLab URL
    'repo' => '25897393' // Project ID
] );