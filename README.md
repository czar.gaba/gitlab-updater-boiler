# gitlab-updater-boiler

# Resource
`https://github.com/krafit/wp-gitlab-updater`

# Technical Requirements

```
- access token
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

- git tag
https://git-scm.com/book/en/v2/Git-Basics-Tagging

- to view git tag in gitlab using Web UI
Git Dashboard ( sidebar ) > Repository > Tags

- version from plugin base name must match git tag example 	
 * Version:           1.0.14

```



# Author

<h3>Czar G. | XIV</h3> 
<h3>Head Technical | MDH NW | Dev </h3> 
<h3><a href="https://mydevhouse.dev">https://mydevhouse.dev</a></h3> <br>
<a href="https://www.facebook.com/appleideath" target="_blank"><img src="https://img.icons8.com/doodle/50/000000/facebook-new.png"></a>
<a href="https://www.reddit.com/r/appleideath" target="_blank"><img src="https://img.icons8.com/doodle/48/000000/reddit--v4.png"></a>
<a href="https://twitter.com/appleideathxx" target="_blank"><img src="https://img.icons8.com/cotton/48/000000/twitter.png"></a>
<a href="https://www.instagram.com/appleideath/" target="_blank"><img src="https://img.icons8.com/officel/64/000000/instagram-new.png"></a>
<a href="https://gitlab.com/czar.gaba" target="_blank"><img src="https://img.icons8.com/color/48/000000/gitlab.png"></a>
<a href="https://www.linkedin.com/in/czar-gaba-b67a24124/" target="_blank"><img src="https://img.icons8.com/doodle/48/000000/linkedin--v2.png"></a>

<p> Love is the Death of Duty</p>


